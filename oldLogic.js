const boardWinningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

const boardCurrentPosition = [
    {id:0, value: 'x'}, {id:1, value: 'o'}, {id:2, value: 'o'},
    {id:3, value: 'x'}, {id:4, value: 'o'}, {id:5, value: ''},
    {id:6, value: 'x'}, {id:7, value: ''}, {id:8, value: ''}]
    
let boardPositionX = boardCurrentPosition.filter(item =>item.value == 'x');
let boardPositionXString = boardPositionX.map(item=>item.id);

let boardPositionO = boardCurrentPosition.filter(item =>item.value == 'o');
let boardPositionOString = boardPositionO.map(item=>item.id);

console.log(boardPositionXString);
console.log(boardPositionOString);

const winnerCheck = (a, b) => {
    return JSON.stringify(a) != JSON.stringify(b);
}

let winnerX;
let i=0
       for ( i=0; winnerCheck(boardPositionXString, boardWinningCombinations[i]); i++) {
        if (i<7) winnerX=true;
        else {
            winnerX = false;
            break;
            }}
            
let winnerO;
let j=0
    for ( j=0; winnerCheck(boardPositionOString, boardWinningCombinations[j]); j++) {
        if (j<7) winnerO=true;
        else {
            winnerO = false;
            break;
            }}

let draw;
let winnerGame;

if ((winnerX == true) & (winnerO == true)) {
    draw=true;
    alert('Ничья')
}
else if ((winnerX == false) & (winnerO == false)) {
    draw = true;
    alert('Ничья')
}
else if ((winnerX == true) & (winnerO == false)) {
    winnerGame = 'Игрок X';
    alert('Игрок Х победил!')
}
else {
    winnerGame = 'Игрок О';
    alert('Игрок О победил!')
}

