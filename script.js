let board = [
    ['', '', ''],
    ['', '', ''],
    ['', '', '']
  ];
  let mode = 'X';

  let state = document.querySelectorAll('td');
  for (let i = 0; i < state.length; i++) {
    state[i].addEventListener('click', function(setMode) {
      let row = Math.floor(i / 3);
      let col = i % 3;
      if (board[row][col] == '') {
        board[row][col] = mode;
        mode = mode == 'X' ? 'O' : 'X';
        FieldCellValue ();
      }
    });
  }

  function FieldCellValue () {
    for (let i = 0; i < state.length; i++) {
      let row = Math.floor(i / 3);
      let col = i % 3;
      state[i].textContent = board[row][col];
    }
  }

  isOverGame
  getGameFieldStatus
  setMode
